const Traineer = {
  name: "Jayson Roda",
  age: 25,
  pokemon: ["Mewtwo", "Metapod", "Caterpie", "Pidgey"],
  friends: {
    hoenn: ["May", "Max"],
    kanto: ["Brock", "Misty"],
  },
  talk: function () {
    console.log(`${this.pokemon[0]}! I choose you!`);
  },
};
console.log("Result of dot notation:");
console.log(Traineer.name);

function Pokemon(name, level) {
  // Properties Pokemon
  this.pokemonName = name;
  this.pokemonLevel = level;
  this.pokemonHealth = 2 * level;
  this.pokemonAttack = level;

  // methods
  // We re going to add a method named tackle
  this.tackle = function (targetPokemon) {
    console.log(`${this.pokemonName} tackles ${targetPokemon.pokemonName}`);
    console.log(
      `${targetPokemon.pokemonName} is now reduced to ${
        targetPokemon.pokemonHealth - this.pokemonAttack
      }`
    );
    targetPokemon.pokemonHealth -= this.pokemonAttack;
    if (targetPokemon.pokemonHealth <= 0)
      this.fainted(targetPokemon.pokemonName);
  };

  this.fainted = function (targetPokemon) {
    console.log(`${targetPokemon} fainted!`);
  };
}
const mewtwo = new Pokemon(Traineer.pokemon[0], 12);
console.log(mewtwo);
const geodude = new Pokemon("Geodude", 20);
console.log(geodude);
mewtwo.tackle(geodude);
mewtwo.tackle(geodude);
mewtwo.tackle(geodude);
mewtwo.tackle(geodude);
